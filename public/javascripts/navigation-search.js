;(function($) {
        function removeCollapsible(id) {
            $('#'+id)
        }

        $(function() {
            $(document).on('click', '.vpath-history-item .delete-btn', function(e) {
                e.preventDefault();
                var $btn = $(this),
                    $item = $btn.closest('.vpath-history-item');

                $.ajax({
                    type: $btn.data("method") || 'GET',
                    url: $btn.attr('href'),
                    success: function() {
                        var items = $item.closest("table").find('.vpath-history-item');
                        if (items.length - 1 <= 0) {
                            var $collapse = $item.closest('.in');
                            var $collapseBtn = $('[data-target="#' + $collapse.attr('id') + '"]');
                            $collapse.remove();
                            $collapseBtn.remove();
                        } else {
                            $item.remove();
                        }
                    }
                });
            });
        });

    $(function() {
        $("#selectRowValue").change(function (e) {
            e.preventDefault();
            window.location = $(this).children('option:selected').data('url');
        });
    });


    $(function() {
        $("#filterTrashedBlocks").click(function(e) {
            e.preventDefault();
            $("#navigation-search-form").submit();
        });
    });

    $(function() {
        $(document).on('click', '#pathItem-btn-add', function(e) {
            e.preventDefault();
            var $btn = $(this);
            var itemId = $btn.data("item-id");
            var formId = "#modalNavItemForm-"+itemId;
            var vpathListId = "#vpath-history-"+itemId;
            var add_vpath_error_container = "#vpath_error_container-"+itemId;
            var vpath_list_container = "#vpath_list_container-"+itemId;
            var url = $(formId).attr("action");
            var table = $(vpathListId).find('table');
            var item =  $(vpathListId).find('.vpath-history-item');
            $(formId).ajaxSubmit({url: url, type: 'POST', success: function(response) {
            if(response.status == 'ok') {
                $(add_vpath_error_container).hide();
                $(vpath_list_container).html(response.content);
                $(vpathListId).collapse();
            } else {
                $(add_vpath_error_container).show();
                $(add_vpath_error_container).html(response.message);
            }
                }
            });
        });
    });
}(jQuery));
