/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;

import java.sql.Connection;

import static org.junit.Assert.assertTrue;

/**
 * Runs tests to prove the database connection stability
 */
public class DatabaseConnectionTest {

    private Database database;

    @Before
    public void setupDatabase() {
        database = Databases.inMemoryWith("jndiName", "DefaultDS");
    }

    @After
    public void shutdownDatabase() {
        database.shutdown();
    }

    @Test
    public void testIsDatabaseConnectionReached() throws Exception {
        try(Connection connection = database.getConnection()) {
            connection.getCatalog();

            assertTrue(connection.getMetaData().getDatabaseProductName().contains("H2"));
            assertTrue(connection.getMetaData().getSchemas().next());
        }
    }

}
