/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.mgt.DefaultSubjectFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;

import play.mvc.Http;
import ch.insign.playauth.shiro.subject.PlayShiroSubjectContext;
import ch.insign.playauth.shiro.subject.support.PlayShiroDelegatingSubject;

public class DefaultPlayShiroSubjectFactory extends DefaultSubjectFactory  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSubjectFactory.class);

    public DefaultPlayShiroSubjectFactory() {
        super();
    }

    @Override
    public Subject createSubject(SubjectContext context) {
        if (!(context instanceof PlayShiroSubjectContext)) {
            return super.createSubject(context);
        }

        PlayShiroSubjectContext sc = (PlayShiroSubjectContext) context;

        SecurityManager securityManager = sc.resolveSecurityManager();
        Session session = sc.resolveSession();
        boolean sessionEnabled = sc.isSessionCreationEnabled();
        PrincipalCollection principals = sc.resolvePrincipals();
        boolean authenticated = sc.resolveAuthenticated();
        String host = sc.resolveHost();
        Http.Context httpContext = sc.resolveHttpContext();

        return new PlayShiroDelegatingSubject(principals, authenticated, host,
                session, sessionEnabled, httpContext, securityManager);
    }
}
