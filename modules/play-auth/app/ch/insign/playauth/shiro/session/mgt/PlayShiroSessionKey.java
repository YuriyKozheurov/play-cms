/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.playauth.shiro.session.PlayShiroSession;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import play.mvc.Http;

import java.io.Serializable;

public class PlayShiroSessionKey extends DefaultSessionKey implements ch.insign.playauth.shiro.util.HttpContextSource {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroSessionKey.class);

    private static final long serialVersionUID = -510753928849558123L;

    private final Http.Context context;

    public PlayShiroSessionKey(Serializable sessionId, Http.Context context) {
        this(context);
        if (null != sessionId) {
            setSessionId(sessionId);
        }
    }

    public PlayShiroSessionKey(Http.Context context) {
        this.context = context;
        if (context != null) {
            setSessionId(PlayShiroSession.getId(context.session()));
        } else {
            setSessionId(PlayShiroSession.generateId());
        }
    }

    @Override
    public Http.Context getHttpContext() {
        return context;
    }
}
