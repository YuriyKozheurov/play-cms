/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.CMS;
import ch.insign.cms.models.NavigationItem;
import ch.insign.cms.models.PageBlock;
import ch.insign.cms.permissions.ApplicationPermission;
import com.google.inject.Inject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.forms.MyLogin;
import ch.insign.playauth.PlayAuth;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import java.util.List;
import java.util.Optional;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class AuthController extends Controller  {
    public static final String BACK_URL = "backUrl";
    public static Form<MyLogin> LOGIN_FORM;
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private static final String JUST_LOGGEDIN_KEY = "just-loggedin";
    private static final String JUST_LOGGEDOUT_KEY = "just-loggedout";
    private static final String JUST_UNAUTHORIZED_KEY = "just-unauthorized";

    private MessagesApi messagesApi;

    @Inject
    public AuthController(MessagesApi messagesApi, FormFactory formFactory) {
        this.messagesApi = messagesApi;
        LOGIN_FORM = formFactory.form(MyLogin.class);
    }

    public Result login() {

        Form<MyLogin> filledForm = LOGIN_FORM.bindFromRequest();

        if (filledForm.hasErrors()) {
            return LoginResult.formError(filledForm);
        }

        UsernamePasswordToken token = new UsernamePasswordToken(filledForm.get().getEmail(), filledForm.get()
                .getPassword());
        if (filledForm.get().rememberme != null) {
            token.setRememberMe(true);
        }

        try {
            PlayAuth.login(token);
        } catch (LockedAccountException lae) {
            // account for that username is locked / not verified - can't login.
            filledForm.reject(messagesApi.get(lang(), "auth.login.error.not_verified"));
            return LoginResult.authError(filledForm);
        } catch (AuthenticationException e) {
            filledForm.reject(messagesApi.get(lang(), "auth.login.error.invalid_credentials"));
            return LoginResult.authError(filledForm);
        }

        setAsJustLoggedIn(Http.Context.current());
        session().put("username", PlayAuth.getPartyIdentifier().orElse("anonymous"));
        String lang = Controller.lang().language();

        String backUrl = flash().get(BACK_URL);
        if (request().body().asFormUrlEncoded().get(BACK_URL) != null && backUrl == null) {
            backUrl = request().body().asFormUrlEncoded().get(BACK_URL)[0];
        }

        if (backUrl != null) {
            return LoginResult.redirect(Optional.ofNullable(NavigationItem.find.byVpath(backUrl))
                    .map(NavigationItem::getPage)
                    .map(p -> p.getNavItem(lang))
                    .map(NavigationItem::getURL)
                    .orElse(backUrl));
        } else {
            if (Http.Context.current().request().path().startsWith("/admin")) {
                return LoginResult.redirect(routes.AdminController.index().url());
            } else {
                return LoginResult.redirect(Optional.ofNullable(PageBlock.find.byKey(PageBlock.KEY_FRONTEND))
                        .map(p -> p.getNavItem(lang))
                        .map(NavigationItem::getURL)
                        .orElse("/"));
            }
        }
    }

    public Result logout(String backURL) {
        logger.info("Fake logout");

        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        setAsJustLoggedOut(Http.Context.current());
        session().remove("username");
        return redirect(backURL);
    }

    public Boolean hasJustLoggedIn(final Http.Context ctx) {
        if (!SecurityUtils.getSubject().isAuthenticated()) {
            return false;
        }
        final String flag = ctx.flash().get(JUST_LOGGEDIN_KEY);
        return flag != null && flag.equals("yes");
    }

    public void setAsJustLoggedIn(final Http.Context ctx) {
        ctx.flash().put(JUST_LOGGEDIN_KEY, "yes");
    }

    public static Boolean hasJustLoggedOut(final Http.Context ctx) {
        if (SecurityUtils.getSubject().isAuthenticated()) {
            return false;
        }
        final String flag = ctx.flash().get(JUST_LOGGEDOUT_KEY);
        return flag != null && flag.equals("yes");
    }

    public void setAsJustLoggedOut(final Http.Context ctx) {
        ctx.flash().put(JUST_LOGGEDOUT_KEY, "yes");
    }

    public Boolean hasJustUnauthorized(final Http.Context ctx) {
        final String flag = ctx.flash().get(JUST_UNAUTHORIZED_KEY);
        return flag != null && flag.equals("yes");
    }

    public Boolean hasJustUnauthorized() {
        return hasJustUnauthorized(ctx());
    }

    public static void setAsJustUnauthorized(final Http.Context ctx) {
        ctx.flash().put(JUST_UNAUTHORIZED_KEY, "yes");
    }

    /**
     * Check the current user who tries to access to filemanager for accessible permission
     * @return successful result if the user has permissions and an error result if not
     */
    public Result authResponsiveFilemanager() throws IllegalAccessException {
        // Check is the filemanager enabled in system
        if(!CMS.getConfig().isResponsiveFileManagerEnable()) {
            throw new IllegalAccessException("The filemanager you want to access isn't enabled. " +
                    "Please ensure your config file supports it");
        }

        ObjectNode result = Json.newObject();
        ObjectNode params = Json.newObject();

        // Check current user for specific permission to access the filemanager
        if (PlayAuth.isPermitted(ApplicationPermission.BROWSE_FILEMANAGER)) {
            String currentUserName = PlayAuth.getCurrentParty().getEmail();
            params.put("user", currentUserName);
            result.set("success", params);
        } else {
            params.put("message", "You don't have permissions to access filemanager.");
            params.put("code", "180");
            result.set("error", params);
        }

        return ok(result);
    }

    private static class LoginResult  {
        public static Result ok() {
            ObjectNode result = Json.newObject();
            result.put("status", "ok");
            return play.mvc.Results.ok(result);
        }

        public static Result redirect(final String uri) {
            ObjectNode result = Json.newObject();
            result.put("status", "redirect");
            result.put("content", uri);

            return play.mvc.Results.ok(result);
        }

        public static Result formError(final Form<MyLogin> form) {
            for (List<ValidationError> errorList : form.errors().values()) {
                for (ValidationError error : errorList) {
                    logger.debug(error.key() + " = " + error.message());
                }
            }

            String backUrl = "/";

            ObjectNode result = Json.newObject();
            result.put("status", "error");
            result.set("errors", form.errorsAsJson());
            result.put("content", ch.insign.cms.views.html._loginFormModal.render(form, backUrl, "").toString());

            return play.mvc.Results.ok(result);
        }

        public static Result authError(final Form<MyLogin> form) {
            return formError(form);
        }
    }

}
