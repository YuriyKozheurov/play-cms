/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.BlockCache;
import ch.insign.cms.permissions.CachePermission;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.utils.CacheUtils;
import com.google.inject.Inject;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.apache.shiro.crypto.hash.Md5Hash;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class CacheController extends Controller {
	private MessagesApi messagesApi;

	@Inject
	public CacheController(MessagesApi messagesApi) {
		this.messagesApi = messagesApi;
	}

	public Result index() {
        PlayAuth.requirePermission(CachePermission.BROWSE);
        return ok(ch.insign.cms.views.html.admin.cacheOverview.render(cacheManagers()));
	}

	public Result clear(String cacheId) {
        PlayAuth.requirePermission(CachePermission.CLEAN);
        Optional<Cache> cache = caches().stream()
				.filter(c -> c.id().equals(cacheId))
				.map(CacheView::cache)
				.findFirst();

		if (cache.isPresent()) {
			cache.ifPresent(Cache::removeAll);
			flash("success", messagesApi.get(lang(), "admin.cache.overview.clear.success.msg", cache.get().getName()));
		} else {
			flash("error", messagesApi.get(lang(), "admin.cache.overview.clear.fail.msg", cacheId));
		}

		return redirect(routes.CacheController.index());
	}

    public Result clearAll() {
        PlayAuth.requirePermission(CachePermission.CLEAN);
        caches().stream()
                .map(CacheView::cache)
                .filter(c -> c != null)
                .forEach(Cache::removeAll);
        flash("success", messagesApi.get(lang(), "admin.cache.overview.clear.caches.success.msg"));

        return redirect(routes.CacheController.index());
    }


    public Result setStatisticsEnabled(String cacheId, Boolean enabled) {
		Optional<Cache> cache = caches().stream()
				.filter(c -> c.id().equals(cacheId))
				.map(CacheView::cache)
				.findFirst();

		if (cache.isPresent()) {
			cache.ifPresent(c -> c.setStatisticsEnabled(enabled));

			if (enabled) {
				flash("success", messagesApi.get(lang(), "admin.cache.overview.statsOn.success.msg", cache.get().getName()));
			} else {
				flash("success", messagesApi.get(lang(), "admin.cache.overview.statsOff.success.msg", cache.get().getName()));
			}
		} else {
			if (enabled) {
				flash("error", messagesApi.get(lang(), "admin.cache.overview.statsOn.fail.msg", cacheId));
			} else {
				flash("error", messagesApi.get(lang(), "admin.cache.overview.statsOff.fail.msg", cacheId));
			}
		}

		return redirect(routes.CacheController.index());
	}

	/**
	 * Return views of known cache managers
	 */
	private List<CacheManagerView> cacheManagers() {
		return Stream.of(
				BlockCache.cache().getCacheManager(),
				CacheUtils.manager())
				.map(manager -> (CacheManagerView) () -> manager)
				.collect(toList());
	}

	/**
	 * Return views of known caches
	 */
	private List<CacheView> caches() {
		return  cacheManagers()
                .stream()
				.flatMap(manager -> manager.caches().stream())
				.collect(toList());
	}

	/**
	 * View of CacheManager.
	 * Intended for convenient use in scala templates.
	 */
	public interface CacheManagerView {

		CacheManager manager();

		default String name() {
			return manager().getName();
		}

		default String config() {
			return manager().getActiveConfigurationText();
		}

		default List<CacheView> caches() {
			return Stream.of(manager().getCacheNames())
					.map(name -> manager().getCache(name))
					.map(cache -> (CacheView) () -> cache)
					.collect(toList());
		}
	}

	private static String cacheSizeInMB(long size) {
		if (size <= 0) {
			return "0";
		}

		int formatForMB = (int) (Math.log10(size) / Math.log10(1024));

		return new DecimalFormat("#.###").format(size / Math.pow(1024, formatForMB));
	}

	/**
	 * View of Cache.
	 * Intended for convenient use in scala templates.
	 */
	public interface CacheView {

		Cache cache();

		default String id() {
			return new Md5Hash(cache().getGuid()).toString();
		}

		default String name() {
			return cache().getName();
		}

		default String config() {
			return cache().getCacheManager().getActiveConfigurationText(cache().getName());
		}

		default boolean hasStats() {
			return cache().isStatisticsEnabled();
		}

		default long hitCount() {
			return cache().getLiveCacheStatistics().getCacheHitCount();
		}

		default long missCount() {
			return cache().getLiveCacheStatistics().getCacheMissCount();
		}

		default long missCountExpired() {
			return cache().getLiveCacheStatistics().getCacheMissCountExpired();
		}

		default long hitRatio() {
			return cache().getLiveCacheStatistics().getCacheHitRatio();
		}

		default long size() {
			return cache().getLiveCacheStatistics().getSize();
		}

	    default String getLocalHeapSizeInBytes() {
            return cacheSizeInMB((cache().getLiveCacheStatistics().getLocalHeapSizeInBytes()));
        }

        default String getLocalOffHeapSizeInBytes() {
            return cacheSizeInMB((cache().getLiveCacheStatistics().getLocalOffHeapSizeInBytes()));
        }

        default String getLocalDiskSizeInBytes() {
            return cacheSizeInMB((cache().getLiveCacheStatistics().getLocalDiskSizeInBytes()));
        }

        default long getLocalHeapSize() {
            return cache().getLiveCacheStatistics().getLocalHeapSize();
        }

        default long getLocalDiskSize() {
            return cache().getLiveCacheStatistics().getLocalDiskSize();
        }

        default long getLocalOffHeapSize() {
            return cache().getLiveCacheStatistics().getLocalOffHeapSize();
        }

        default long evictedCount() {
			return cache().getLiveCacheStatistics().getEvictedCount();
		}

		default long putCount() {
			return cache().getLiveCacheStatistics().getPutCount();
		}

		default long updateCount() {
			return cache().getLiveCacheStatistics().getUpdateCount();
		}

		default long expiredCount() {
			return cache().getLiveCacheStatistics().getExpiredCount();
		}

		default long removedCount() {
			return cache().getLiveCacheStatistics().getRemovedCount();
		}
	}

}
