/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class AjaxResult {

    private static final String STATUS_OK = "ok";
    private static final String STATUS_REDIRECT = "redirect";
    private static final String STATUS_ERROR = "error";
    private static final String STATUS_UNAUTHORIZED = "unauthorized";

    public static Result ok(final String content) {
        return ok(Optional.ofNullable(content), Optional.empty());
    }

    public static Result ok(final JsonNode extra) {
        return ok(Optional.empty(), Optional.ofNullable(extra));
    }

    public static Result ok(final String content, final JsonNode extra) {
        return ok(Optional.ofNullable(content), Optional.ofNullable(extra));
    }

    private static Result ok(final Optional<String> message, final Optional<JsonNode> extra) {
        ObjectNode result = getStatusNode(STATUS_OK);
        message.ifPresent(m -> result.put("message", m));
        extra.ifPresent(e -> result.set("extra", e));

        return play.mvc.Results.ok(result);
    }

    public static Result ok() {
        return play.mvc.Results.ok(getStatusNode(STATUS_OK));
    }

    public static Result redirect(String url) {
        return redirect(url, Collections.emptyMap());
    }

    public static Result redirect(String url, Map<String, String> content) {
        ObjectNode result = getStatusNode(STATUS_REDIRECT);
        result.put("url", url);

        for (String key : content.keySet()) {
            result.put(key, content.get(key));
        }

        return play.mvc.Results.ok(result);
    }

    public static Result error(final String message) {
        ObjectNode result = getStatusNode(STATUS_ERROR);
        result.put("message", message);

        return play.mvc.Results.ok(result);
    }

    public static <T> Result error(final Form<T> form) {
        ObjectNode result = getStatusNode(STATUS_ERROR);
        result.set("errors", form.errorsAsJson());

        return play.mvc.Results.ok(result);
    }

    public static Result error(JsonNode node) {
        ObjectNode result = getStatusNode(STATUS_ERROR);
        result.set("errors", node);

        return play.mvc.Results.ok(result);
    }

    public static Result unauthorized(String loginModalUrl) {
        ObjectNode result = getStatusNode(STATUS_UNAUTHORIZED);
        result.put("url", loginModalUrl);

        return play.mvc.Results.ok(result);
    }

    private static ObjectNode getStatusNode(String status) {
        ObjectNode node = Json.newObject();
        node.put("status", status);
        return node;
    }
}
