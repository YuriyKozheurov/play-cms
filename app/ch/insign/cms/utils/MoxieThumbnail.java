/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.utils;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.models.MoxiemanagerConfiguration;
import org.apache.commons.io.FileUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.Info;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;


/**
 * Class to create thumbnails
 */
public class MoxieThumbnail  {
	private final static Logger logger = LoggerFactory.getLogger(MoxieThumbnail.class);

    private static String THUMBNAIL_PATH = "System/thumbnails";

    public static String getThumbnailPath(String relativePath, Integer maxWidth, Integer maxHeight, double quality) {
        int width = 0;
        int height = 0;
        if (maxWidth != null)
            width = maxWidth;
        if (maxHeight != null)
            height = maxHeight;
        return "/moxiemanager/data/files/" + THUMBNAIL_PATH + File.separator + width + "x" + height + "." + quality + File.separator + relativePath.substring(25);
    }

    public static String getThumbnailAbsolutePath(String relativePath, Integer maxWidth, Integer maxHeight, double quality) {
        String moxiePath = new MoxiemanagerConfiguration().getWWWRoot();
        return moxiePath + getThumbnailPath(relativePath, maxWidth, maxHeight, quality);
    }

    public static String createThumbnail(String relativePath, Integer maxWidth, Integer maxHeight, double quality) throws IOException, InterruptedException, IM4JavaException {
        relativePath = URLDecoder.decode(relativePath, "UTF-8");

        String moxiePath = new MoxiemanagerConfiguration().getPhysicalRoot();
        File imageFile = new File(moxiePath + relativePath);
        logger.debug("imageFile: " + imageFile.getAbsoluteFile());
        if (imageFile.isFile()) {
            Info info = new Info(imageFile.getAbsolutePath());
            if ((maxWidth != null && info.getImageWidth() > maxWidth) || (maxHeight != null && info.getImageHeight() > maxHeight)) {
                File thumbnail;
                if (relativePath.startsWith("/moxiemanager/data/files/")) {
                    String thumbnailRelativePath = getThumbnailPath(relativePath, maxWidth, maxHeight, quality);
                    thumbnail = new File(moxiePath + thumbnailRelativePath);
                    final File tmpFile = File.createTempFile("thumbnail-", ".tmp");
                    ConvertCmd cmd = new ConvertCmd();
                    IMOperation op = new IMOperation();
                    op.resize(maxWidth, maxHeight);
                    op.quality(quality);
                    op.addImage(imageFile.getAbsolutePath());
                    op.addImage(tmpFile.getAbsolutePath());
                    cmd.setAsyncMode(false);
                    cmd.run(op);
                    play.Logger.debug("thumbnailRelativePath: " + thumbnailRelativePath);
                    logger.debug("thumbnailRelativePath: " + thumbnailRelativePath);
                    if (thumbnail.getParentFile().isDirectory() || thumbnail.getParentFile().mkdirs()) {
                        FileUtils.copyFile(tmpFile, thumbnail);
                        FileUtils.deleteQuietly(tmpFile);
                        return thumbnailRelativePath;
                    } else {
                        throw new IOException("The directory '" + thumbnail.getParent() + "' could not be created recursively.");
                    }
                } else {
                    throw new IOException("Expected relativePath to start with '/moxiemanager/data/files/'.");
                }
            }
        }
        return relativePath;
    }

    
    public static void optimizeImage(String absolutePathFrom, String absolutePathTo, Integer maxWidth, Integer maxHeight, double quality) throws IOException, InterruptedException, IM4JavaException{
        File imageFile = new File(absolutePathFrom);
        logger.debug("imageFile: " + imageFile.getAbsoluteFile());
        if (imageFile.isFile()) {
            Info info = new Info(imageFile.getAbsolutePath());
            if ((maxWidth != null && info.getImageWidth() > maxWidth) || (maxHeight != null && info.getImageHeight() > maxHeight)) {
                if (!absolutePathFrom.contains("/moxiemanager/data/files/")) 
                	throw new IOException("Expected from-path to contain '/moxiemanager/data/files/'.");
                if (!absolutePathTo.contains("/moxiemanager/data/files/")) 
                	throw new IOException("Expected to-path to contain '/moxiemanager/data/files/'.");
                
                final File tmpFile = File.createTempFile("thumbnail-", ".tmp");
                File thumbnail = new File(absolutePathTo);
                ConvertCmd cmd = new ConvertCmd();
                IMOperation op = new IMOperation();
                op.resize(maxWidth, maxHeight);
                op.quality(quality);
                op.addImage(absolutePathFrom);
                op.addImage(tmpFile.getAbsolutePath());
                cmd.setAsyncMode(false);
                cmd.run(op);
                if (thumbnail.getParentFile().isDirectory() || thumbnail.getParentFile().mkdirs()) {
                    FileUtils.copyFile(tmpFile, thumbnail);
                    FileUtils.deleteQuietly(tmpFile);
                } else {
                    throw new IOException("The directory '" + thumbnail.getParent() + "' could not be created recursively.");
                }
                logger.debug("Optimized "+absolutePathFrom +" into "+absolutePathTo);
            }
        }
    }
}
