/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.formatters;

import org.springframework.core.convert.TypeDescriptor;
import play.i18n.MessagesApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Optional;

@Singleton
public class Formatters extends play.data.format.Formatters{

    @Inject
    public Formatters(MessagesApi messagesApi) {
        super(messagesApi);
    }

    @Override
    public <A extends Annotation, T> play.data.format.Formatters register(Class<T> clazz, play.data.format.Formatters.AnnotationFormatter<A, T> formatter) {
        return super.register(clazz, formatter);
    }

    @Override
    public <T> String print(T t) {
        return super.print(unwrap(t));
    }

    @Override
    public <T> String print(Field field, T t) {
        return super.print(field, unwrap(t));
    }

    @Override
    public <T> String print(TypeDescriptor desc, T t) {
        return super.print(desc, unwrap(t));
    }

    @SuppressWarnings("unchecked")
    private <T> T unwrap(T t) {
        if (t instanceof Optional<?>) {
            return (T) ((Optional) t).orElse(null);
        }

        return t;
    }
}
