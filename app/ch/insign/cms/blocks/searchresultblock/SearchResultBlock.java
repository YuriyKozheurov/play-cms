/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.searchresultblock;

import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.ContentBlock;
import ch.insign.commons.search.SearchQuery;
import ch.insign.commons.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * This block will show search results from a query (sent by the default ?query=keywords) to
 * Cms.getSearchManager.query()
 */
@Entity
@Table(name = "cms_block_searchresult")
@DiscriminatorValue("SearchResultBlock")
//@SecuredEntity(name="SearchResultBlock", actions = {"read", "modify"})
public class SearchResultBlock extends ContentBlock  {
	private final static Logger logger = LoggerFactory.getLogger(SearchResultBlock.class);

    public static BlockFinder<SearchResultBlock> find = new BlockFinder<>(SearchResultBlock.class);

    private int maxResults = 20;

    public int getMaxResults() {
        return maxResults;
    }
    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    @Override
    public AbstractBlock.BlockType getType() {
        return BlockType.BLOCK;
    }

    @Override
    public Html render() {
        String query = Controller.request().getQueryString("query");
        SearchQuery sq = new SearchQuery(query);
        sq.limit = getMaxResults();

        List<SearchResult> results = CMS.getSearchManager().search(sq);

        return ch.insign.cms.blocks.searchresultblock.html.searchResultShow.render(this, query, results);
    }

    @Override
    public Html editForm(Form editForm) {
        String backURL = Controller.request().getQueryString("backURL");
        return ch.insign.cms.blocks.searchresultblock.html.searchResultEdit.render(this, editForm, backURL, null);
    }
}

