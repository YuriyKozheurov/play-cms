/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.inject;

import ch.insign.cms.models.partyrole.view.PartyRoleCreateView;
import ch.insign.cms.models.partyrole.view.PartyRoleEditView;
import ch.insign.cms.models.partyrole.view.PartyRoleListView;
import ch.insign.cms.models.partyrole.view.support.DefaultPartyRoleCreateView;
import ch.insign.cms.models.partyrole.view.support.DefaultPartyRoleEditView;
import ch.insign.cms.models.partyrole.view.support.DefaultPartyRoleListView;
import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import scala.collection.Seq;

public class PartyRoleViewModule extends play.api.inject.Module {
    @Override
    public Seq<Binding<?>> bindings(Environment environment, Configuration configuration) {
        return seq(
                bind(PartyRoleCreateView.class).to(DefaultPartyRoleCreateView.class),
                bind(PartyRoleEditView.class).to(DefaultPartyRoleEditView.class),
                bind(PartyRoleListView.class).to(DefaultPartyRoleListView.class));
    }
}
