/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.validator;

import play.data.Form;
import play.data.validation.ValidationError;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract form validator class, that allows for subclass validators to be injected and check custom validation rules
 * @param <T> The form class type
 */
public abstract class FormValidator<T> {

    /**
     * Validate the form against this validator and add all errors to the form errors
     *
     * @param form The play form wrapping around the form entity
     * @return The same play form with any custom validation errors
     */
    public Form<T> validate(Form<T> form) {
        if (!form.hasErrors()) {
            List<ValidationError> customErrors = validate(form.get());
            form.errors().putAll(toMap(customErrors));
        }

        return form;
    }

    /**
     * Override this method for your custom validation rules
     *
     * @param form The form value
     * @return A list of validation errors
     */
    protected abstract List<ValidationError> validate(T form);

    /**
     * Helper method to transform a list of validation errors into a map, where the map key is the same as the error key
     *
     * @param errors A list of ValidationErrors
     * @return A map where each validation error is one entry
     */
    private Map<String, List<ValidationError>> toMap(List<ValidationError> errors) {
        Map<String, List<ValidationError>> errorMap = new HashMap<>();
        errors.forEach(error -> errorMap.put(error.key(), Collections.singletonList(error)));
        return errorMap;
    }

}
