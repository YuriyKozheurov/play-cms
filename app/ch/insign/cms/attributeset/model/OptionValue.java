/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="cms_eav_attribute_option_value")
@DiscriminatorValue("option")
public class OptionValue extends Value {

    @ManyToMany
    private List<Option> value = new ArrayList<>();

    // ======= Getters and Setters =======

    public List<Option> getValue() {
        return value;
    }

    public void setValue(List<Option> value) {
        this.value = value;
    }

    public void addValue(Option option) {
        this.value.add(option);
    }
}
