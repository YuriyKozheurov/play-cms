/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;

import java.util.HashMap;
import java.util.function.Supplier;

/**
 * Created by bachi on 15.07.14.
 */
public class UncachedManager {
	private final static Logger logger = LoggerFactory.getLogger(UncachedManager.class);

	protected HashMap<String, Supplier<Html>> uncachedPartials = new HashMap<>();

	public UncachedManager register(String key, Supplier<Html> supplier) {
		if (uncachedPartials.containsKey(key)) {
			throw new RuntimeException("Invalid uncachedPartial key - already added: " + key);
		}
		uncachedPartials.put(key, supplier);
		logger.debug("Added uncached partial: " + key);
		return this;
	}

	public void flush() {
		uncachedPartials.clear();
	}

	public String parseContent(String cachedContent) {
		for (String key : uncachedPartials.keySet()) {
			String tag = getTag(key);
			if (cachedContent.contains(tag)) {
				cachedContent = cachedContent.replace(tag, uncachedPartials.get(key).get().toString());
			}
		}
		return cachedContent;
	}

	public String getTag(String key) {
		return "[uncached: " + key + "]";
	}

	public boolean contains(String key) {
		return uncachedPartials.containsKey(key);
	}

	/**
	 * Render a single partial
	 * @return Html or "" if not found
	 */
	public Html render(String key) {
		if (uncachedPartials.containsKey(key)) {
			return uncachedPartials.get(key).get();
		} else {
			logger.error("Key not found. Cannot render uncached partial with key: " + key);
			return Html.apply("");
		}
	}
}
